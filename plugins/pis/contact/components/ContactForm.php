<?php
namespace Pis\Contact\Components;
use Cms\Classes\ComponentBase;
use Input;
use Mail;
use DB;
use Flash;

/**
* 
*/
class ContactForm extends ComponentBase
{
	public $paket;

	public function componentDetails(){
		return [
			'name' => 'Contact Form',
			'description' => 'PenidaTrip Contact Form'
		]; 
	}

	// onRun itu handler, jangan diganti
	public function onRun(){
		$this->paket = $this->loadPaket();
	}

	private function loadPaket(){
		return Db::table('pis_site_paket')->get();
	}

	// onSun itu handler, jangan diganti
	public function onSend(){

		$vars = [
			'nama' => Input::get('nama'),
			'paket' => Input::get('pilihpaket'),
			'tanggal' => Input::get('tanggalbooking'),
			'tel' => Input::get('telepon'),
			'email' => Input::get('email'),
			'wni' => Input::get('jumlahwni'),
			'wna' => Input::get('jumlahwna'),
			'keterangan' => Input::get('keterangan')
		];

		Mail::send('pis.contact::mail.message', $vars, function($message) {

		    $message->to('penidatrip@gmail.com', 'Penida Admin');
		    $message->subject('Web Booking Penidatrip');

		});
		$result = [
			'success' => 'true'
		];

		Flash::success('Pesan telah terkirim<br>CS kami akan segera menghubungi anda');

		// return $result;

	}
}