<?php namespace Pis\Contact;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    	return [
    		'Pis\Contact\Components\ContactForm' => 'contactForm'
    	];
    }

    public function registerSettings()
    {
    }
}
