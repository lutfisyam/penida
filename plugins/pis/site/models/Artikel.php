<?php namespace Pis\Site\Models;

use Model;

/**
 * Model
 */
class Artikel extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pis_site_artikel';

    public $attachOne = [
        'page_header' => 'System\Models\File',
        'image_above' => 'System\Models\File'
    ];
}