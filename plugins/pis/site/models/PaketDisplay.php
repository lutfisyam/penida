<?php namespace Pis\Site\Models;

use Model;

/**
 * Model
 */
class PaketDisplay extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pis_site_paket_display';

    public $jsonable = ['content'];

    public $hasOne = [
        'paket' => ['Pis\Site\Models\Paket', 'key' => 'id'] 
    ];

    public function test($a){
        return 'traaaaaaa' . $a;
    }
}