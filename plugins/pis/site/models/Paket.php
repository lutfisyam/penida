<?php namespace Pis\Site\Models;

use Model;

/**
 * Model
 */
class Paket extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /*
     * Validation
     */
    public $rules = [
    ];

    protected $jsonable =['price_list', 'itinerary'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pis_site_paket';

    public $attachOne = [
        'page_header' => 'System\Models\File',
        'thumbnail' => 'System\Models\File'
    ];

    public $attachMany = [
        'slider' => 'System\Models\File'
    ];
}