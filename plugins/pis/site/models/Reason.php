<?php namespace Pis\Site\Models;

use Model;

/**
 * Model
 */
class Reason extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pis_site_reason';

    // relation to image
    public $attachOne = [
        'image' => 'System\Models\File'
    ];
}