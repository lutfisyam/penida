<?php namespace Pis\Site\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Reason extends Controller
{
    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController','Backend\Behaviors\ReorderController'];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Pis.Site', 'main-menu-item', 'side-menu-item4');
    }
}