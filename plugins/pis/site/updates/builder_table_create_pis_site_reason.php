<?php namespace Pis\Site\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePisSiteReason extends Migration
{
    public function up()
    {
        Schema::create('pis_site_reason', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('content');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pis_site_reason');
    }
}
