<?php namespace Pis\Site\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePisSiteGeneric extends Migration
{
    public function up()
    {
        Schema::create('pis_site_generic', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title');
            $table->text('content');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pis_site_generic');
    }
}
