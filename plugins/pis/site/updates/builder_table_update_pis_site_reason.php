<?php namespace Pis\Site\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePisSiteReason extends Migration
{
    public function up()
    {
        Schema::table('pis_site_reason', function($table)
        {
            $table->integer('order')->default(100);
            $table->increments('id')->unsigned(false)->change();
        });
    }
    
    public function down()
    {
        Schema::table('pis_site_reason', function($table)
        {
            $table->dropColumn('order');
            $table->increments('id')->unsigned()->change();
        });
    }
}
