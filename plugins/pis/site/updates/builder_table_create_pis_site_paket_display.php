<?php namespace Pis\Site\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePisSitePaketDisplay extends Migration
{
    public function up()
    {
        Schema::create('pis_site_paket_display', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('name');
            $table->text('content');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pis_site_paket_display');
    }
}
