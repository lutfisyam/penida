<?php namespace Pis\Site\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePisSiteTestimonial extends Migration
{
    public function up()
    {
        Schema::create('pis_site_testimonial', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('city');
            $table->text('content');
            $table->integer('order')->default(100);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pis_site_testimonial');
    }
}
