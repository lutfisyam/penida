<?php namespace Pis\Site\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePisSiteRecordFinder extends Migration
{
    public function up()
    {
        Schema::create('pis_site_record_finder', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('id');
            $table->text('content');
            $table->primary(['id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pis_site_record_finder');
    }
}
