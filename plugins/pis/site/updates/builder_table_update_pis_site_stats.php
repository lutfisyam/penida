<?php namespace Pis\Site\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePisSiteStats extends Migration
{
    public function up()
    {
        Schema::table('pis_site_stats', function($table)
        {
            $table->string('title', 255)->nullable()->change();
            $table->text('content')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('pis_site_stats', function($table)
        {
            $table->string('title', 255)->nullable(false)->change();
            $table->text('content')->nullable(false)->change();
        });
    }
}
