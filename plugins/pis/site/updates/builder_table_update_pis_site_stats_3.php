<?php namespace Pis\Site\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePisSiteStats3 extends Migration
{
    public function up()
    {
        Schema::table('pis_site_stats', function($table)
        {
            $table->dropColumn('deleted_at');
        });
    }
    
    public function down()
    {
        Schema::table('pis_site_stats', function($table)
        {
            $table->timestamp('deleted_at')->nullable();
        });
    }
}
