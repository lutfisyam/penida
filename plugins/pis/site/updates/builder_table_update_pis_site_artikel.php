<?php namespace Pis\Site\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePisSiteArtikel extends Migration
{
    public function up()
    {
        Schema::table('pis_site_artikel', function($table)
        {
            $table->smallInteger('have_sticky');
            $table->string('sticky', 50);
        });
    }
    
    public function down()
    {
        Schema::table('pis_site_artikel', function($table)
        {
            $table->dropColumn('have_sticky');
            $table->dropColumn('sticky');
        });
    }
}
