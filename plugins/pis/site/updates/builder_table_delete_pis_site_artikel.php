<?php namespace Pis\Site\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeletePisSiteArtikel extends Migration
{
    public function up()
    {
        Schema::dropIfExists('pis_site_artikel');
    }
    
    public function down()
    {
        Schema::create('pis_site_artikel', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('title', 255);
            $table->text('content');
            $table->string('slug', 255);
            $table->dateTime('created');
        });
    }
}
