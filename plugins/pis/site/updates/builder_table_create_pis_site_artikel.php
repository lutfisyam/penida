<?php namespace Pis\Site\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePisSiteArtikel extends Migration
{
    public function up()
    {
        Schema::create('pis_site_artikel', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title');
            $table->text('content');
            $table->string('slug');
            $table->dateTime('created');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pis_site_artikel');
    }
}
