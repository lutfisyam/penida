<?php namespace Pis\Site\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePisSiteArtikel2 extends Migration
{
    public function up()
    {
        Schema::table('pis_site_artikel', function($table)
        {
            $table->smallInteger('have_sticky')->default(0)->change();
        });
    }
    
    public function down()
    {
        Schema::table('pis_site_artikel', function($table)
        {
            $table->smallInteger('have_sticky')->default(null)->change();
        });
    }
}
