<?php namespace Pis\Site\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePisSitePaket extends Migration
{
    public function up()
    {
        Schema::create('pis_site_paket', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name')->nullable();
            $table->text('description');
            $table->string('slug')->nullable();
            $table->text('price_list');
            $table->text('itinerary');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pis_site_paket');
    }
}
