$(document).ready(function() {
  // Navbar scroll
  var nav;

  function yScroll() {
    nav = $('.navbar');
    yPos = window.pageYOffset;
    nav.addClass("navbar-transparent");
    if (yPos > 100) {
      nav.removeClass("navbar-transparent");
    }
  }
  window.addEventListener("scroll", yScroll);

  // Search bar
  // Remove Search if user Resets Form or hits Escape!
  $('body, .navbar-collapse form[role="search"] button[type="reset"]').on('click keyup', function(event) {
    console.log(event.currentTarget);
    if (event.which == 27 && $('.navbar-collapse form[role="search"]').hasClass('active') ||
      $(event.currentTarget).attr('type') == 'reset') {
      closeSearch();
    }
  });

  function closeSearch() {
    var $form = $('.navbar-collapse form[role="search"].active')
    $form.find('input').val('');
    $form.removeClass('active');
  }

  // Show Search if form is not active // event.preventDefault() is important, this prevents the form from submitting
  $(document).on('click', '.navbar-collapse form[role="search"]:not(.active) button[type="submit"]', function(event) {
    event.preventDefault();
    var $form = $(this).closest('form'),
      $input = $form.find('input');
    $form.addClass('active');
    $input.focus();

  });
  // ONLY FOR DEMO // Please use $('form').submit(function(event)) to track from submission
  // if your form is ajax remember to call `closeSearch()` to close the search container
  $(document).on('click', '.navbar-collapse form[role="search"].active button[type="submit"]', function(event) {
    event.preventDefault();
    var $form = $(this).closest('form'),
      $input = $form.find('input');
    $('#showSearchTerm').text($input.val());
    closeSearch()
  });

  // kenburnsy
  $(".kenburns-img-wrapper").kenburnsy({
    fullscreen: true, // fullscreen mode
    duration: 9000,
    fadeInDuration: 1500
  });

  // Carousel Lokasi
  var owlPackage = $('.owl-carousel--package');
  owlPackage.owlCarousel({
    autoplay: true,
    loop: true,
    margin: 15,
    nav: false,
    center: true,
    responsive: {
      0: {
        items: 1
      },
      768: {
        items: 3
      },
      1000: {
        items: 3
      }
    }
  });

  // Carousel Lokasi
  var owlLocation = $('.owl-carousel--location');
  owlLocation.owlCarousel({
    autoplay: true,
    loop: true,
    margin: 15,
    nav: false,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 1
      },
      1000: {
        items: 1
      }
    }
  });

  // Carousel Testimonial
  var owlTestimonial = $('.owl-carousel--testimonial');
  owlTestimonial.owlCarousel({
    loop: true,
    margin: 15,
    nav: false,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 1
      },
      1000: {
        items: 1
      }
    }
  });

  // Carousel Photo Detail
  var owlPhoto = $('.owl-carousel--photo');
  owlPhoto.owlCarousel({
    autoplay: true,
    loop: true,
    margin: 0,
    nav: false,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 1
      },
      1000: {
        items: 1
      }
    }
  });

  var owlMinislide = $('.owl-carousel--slide');
  owlMinislide.owlCarousel({
    autoplay: true,
    loop: true,
    margin: 5,
    nav: false,
    center: false,
    dots: false,
    responsive: {
      0: {
        items: 1
      },
      768: {
        items: 3
      },
      1000: {
        items: 3
      }
    }
  });

  // Go to the next item
  $('.next').click(function() {
    owlMinislide.trigger('next.owl.carousel');
  })
  // Go to the previous item
  $('.prev').click(function() {
    owlMinislide.trigger('prev.owl.carousel', [300]);
  })

})
