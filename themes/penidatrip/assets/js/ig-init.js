  var userFeed = new Instafeed({
    get: 'user',
    userId: '5635668021',
    clientId: 'fa6173e645ca4d7ba31baa0032b4f9b6',
    accessToken: '5635668021.fa6173e.c5195dd25e10493b87c67890116ddd3a',
    resolution: 'low_resolution',
    template: '<div class="col-sm-2 col-xs-6"><a href="{{link}}" class="instagram-list"><figure><img src="{{image}}" alt="{{caption}}" class="img-responsive of-image ig-feed-thumb full-width"><figcaption><p><i class="fa fa-heart-o"></i> {{likes}}</p></figcaption></figure></a></div>',
    sortBy: 'most-recent',
    limit: 12,
    links: false
  });
  userFeed.run();